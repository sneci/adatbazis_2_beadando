CREATE TABLE customers_h(
  id                 NUMBER         NOT NULL
  ,first_name        VARCHAR2(40)   NOT NULL
  ,last_name         VARCHAR2(40)   NOT NULL
  ,job               VARCHAR2(40)
  ,email             VARCHAR2(40)
  ,age    NUMBER     NOT NULL
);