CREATE OR REPLACE PACKAGE BODY store_pkg AS
    PROCEDURE prc_store(beers IN alcohol_degree DEFAULT alcohol_degree(2, 3, 4, 5)) IS
    BEGIN
         FOR i IN 1..beers.COUNT LOOP
            DBMS_OUTPUT.PUT_LINE('Beer alcohol degree: ' || beers(i));
        END LOOP;
    END prc_store;

    FUNCTION fnc_store RETURN NUMBER IS
        v_count NUMBER;
    BEGIN
        SELECT COUNT(*)
        INTO v_count
        FROM stores;
        RETURN v_count;
    END fnc_store;
	
    PROCEDURE prc_vw_store_view_sum IS
    BEGIN
        FOR i IN (SELECT * FROM vw_store_view_sum)
        LOOP
            DBMS_OUTPUT.PUT_LINE('Customer ID: ' || i.customerId);
            DBMS_OUTPUT.PUT_LINE('First Name: ' || i.first_name);
            DBMS_OUTPUT.PUT_LINE('Last Name: ' || i.last_name);
            DBMS_OUTPUT.PUT_LINE('Age: ' || i.age);
            DBMS_OUTPUT.PUT_LINE('Store Name: ' || i.store_name);
            DBMS_OUTPUT.PUT_LINE('Address: ' || i.address);
            DBMS_OUTPUT.PUT_LINE('Rate: ' || i.rate);
            DBMS_OUTPUT.PUT_LINE('Beer ID: ' || i.beerId);
            DBMS_OUTPUT.PUT_LINE('Beer Name: ' || i.beer_name);
            DBMS_OUTPUT.PUT_LINE('Beer Type: ' || i.beer_type);
            DBMS_OUTPUT.PUT_LINE('Beer Piece: ' || i.beer_piece);
        END LOOP;
    END prc_vw_store_view_sum;

    FUNCTION fnc_beer_pieces RETURN NUMBER IS
        v_total_pieces NUMBER := 0;
    BEGIN
        FOR rec IN (SELECT beer_piece FROM vw_store_view_sum)
        LOOP
            v_total_pieces := v_total_pieces + rec.beer_piece;
        END LOOP;
        
        RETURN v_total_pieces;
    END fnc_beer_pieces;
END store_pkg;
/