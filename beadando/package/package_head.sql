CREATE OR REPLACE PACKAGE store_pkg AS
    PROCEDURE prc_store(beers IN alcohol_degree DEFAULT alcohol_degree(2, 3, 4, 5));
    FUNCTION fnc_store RETURN NUMBER;
    PROCEDURE prc_vw_store_view_sum;
    FUNCTION fnc_beer_pieces RETURN NUMBER;
END store_pkg;
/