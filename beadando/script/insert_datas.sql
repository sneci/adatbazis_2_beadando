insert into beers (ID, BEER_NAME, BEER_TYPE, BEER_PIECE, CREATED, LAST_MODIFIED)
values (1, 'Staropramen', 'Unfiltered', 10, NULL, NULL);
insert into beers (ID, BEER_NAME, BEER_TYPE, BEER_PIECE, CREATED, LAST_MODIFIED)
values (2, 'Borsosi', 'SemiBrown', 12,  NULL, NULL);
insert into beers (ID, BEER_NAME, BEER_TYPE, BEER_PIECE, CREATED, LAST_MODIFIED)
values (3, 'Soproni', 'Brown', 8,  NULL, NULL);
insert into beers (ID, BEER_NAME, BEER_TYPE, BEER_PIECE, CREATED, LAST_MODIFIED)
values (4, 'Kobanyai', 'Bearing', 6,  NULL, NULL);

insert into stores (ID, STORE_NAME, BEER_ID, CUSTOMER_ID, ADDRESS, RATE)
values (1, 'Pecsi nagyker', 2, 1, 'kis utca 2', 3);
insert into stores (ID, STORE_NAME, BEER_ID, CUSTOMER_ID, ADDRESS, RATE)
values (2, 'Pesti nagyker', 3, 2, 'nagy utca 1', 4);
insert into stores (ID, STORE_NAME, BEER_ID, CUSTOMER_ID, ADDRESS, RATE)
values (3, 'Szegedi nagyker', 4, 3, 'hegy utca 12', 5);
insert into stores (ID, STORE_NAME, BEER_ID, CUSTOMER_ID, ADDRESS, RATE)
values (3, 'Szegedi nagyker', 4, 2, 'hegy utca 12', 5);

INSERT INTO customers (ID, FIRST_NAME, LAST_NAME, JOB, EMAIL, AGE)
VALUES (1, 'Janos', 'Kovacs', 'elado', 'janos@kovacs.gmail.hu', 72);
INSERT INTO customers (ID, FIRST_NAME, LAST_NAME, JOB, EMAIL, AGE)
VALUES (2, 'Jozsef', 'Kiss', 'elado', 'joskiss@gmail.hu', 81);
INSERT INTO customers (ID, FIRST_NAME, LAST_NAME, JOB, EMAIL, AGE)
VALUES (3, 'Bela', 'Nagy', 'uzletvezeto', 'belanagy@gmail.hu', 68);