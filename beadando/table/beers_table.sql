CREATE TABLE beers(
  id         NUMBER         NOT NULL
 ,beer_name        VARCHAR2(40)   NOT NULL
 ,beer_type         VARCHAR2(40)   NOT NULL
 ,beer_piece		NUMBER         NOT NULL
 ,created			DATE
 , last_modified	DATE 
);