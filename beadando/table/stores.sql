CREATE TABLE stores(
  id                NUMBER         NOT NULL
 ,store_name        VARCHAR2(40)   NOT NULL
 ,beer_id           NUMBER         NOT NULL
 ,customer_id       NUMBER         NOT NULL
 ,address           VARCHAR2(255)   NOT NULL
);