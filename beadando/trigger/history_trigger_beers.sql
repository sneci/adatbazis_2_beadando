CREATE OR REPLACE TRIGGER trg_beers_history
AFTER INSERT OR UPDATE OR DELETE ON beers
FOR EACH ROW
DECLARE
    v_action VARCHAR2(10);
BEGIN
    IF INSERTING THEN
        v_action := 'INSERT';
        INSERT INTO beers_h (id, beer_name, beer_type, beer_piece, created)
        VALUES (:NEW.id, :NEW.beer_name, :NEW.beer_type, :NEW.beer_piece, SYSDATE);
    ELSIF UPDATING THEN
        v_action := 'UPDATE';
        INSERT INTO beers_h (id, beer_name, beer_type, beer_piece, created, last_modified)
        VALUES (:NEW.id, :NEW.beer_name, :NEW.beer_type, :NEW.beer_piece, :OLD.created, SYSDATE);
    ELSIF DELETING THEN
        v_action := 'DELETE';
        INSERT INTO beers_h (id, beer_name, beer_type, beer_piece, created, last_modified)
        VALUES (:OLD.id, :OLD.beer_name, :OLD.beer_type, :OLD.beer_piece, :OLD.created, SYSDATE);
    END IF;
END;
/
