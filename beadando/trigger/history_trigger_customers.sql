CREATE OR REPLACE TRIGGER trg_customers_history
AFTER INSERT OR UPDATE OR DELETE ON customers
FOR EACH ROW
DECLARE
    v_action VARCHAR2(10);
BEGIN
    IF INSERTING THEN
        v_action := 'INSERT';
        INSERT INTO customers_h (id, first_name, last_name, job, email, age)
        VALUES (:NEW.id, :NEW.first_name, :NEW.last_name, :NEW.job, :NEW.email, :NEW.age);
    ELSIF UPDATING THEN
        v_action := 'UPDATE';
        INSERT INTO customers_h (id, first_name, last_name, job, email, age)
        VALUES (:NEW.id, :NEW.first_name, :NEW.last_name, :NEW.job, :NEW.email, :NEW.age);
    ELSIF DELETING THEN
        v_action := 'DELETE';
        INSERT INTO customers_h (id, first_name, last_name, job, email, age)
        VALUES (:OLD.id, :OLD.first_name, :OLD.last_name, :OLD.job, :OLD.email, :OLD.age);
    END IF;
END;
/