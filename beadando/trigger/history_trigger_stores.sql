CREATE OR REPLACE TRIGGER trg_stores_history
AFTER INSERT OR UPDATE OR DELETE ON stores
FOR EACH ROW
DECLARE
    v_action VARCHAR2(10);
BEGIN
    IF INSERTING THEN
        v_action := 'INSERT';
        INSERT INTO stores_h (id, store_name, beer_id, customer_id, address)
        VALUES (:NEW.id, :NEW.store_name, :NEW.beer_id, :NEW.customer_id, :NEW.address);
    ELSIF UPDATING THEN
        v_action := 'UPDATE';
        INSERT INTO stores_h (id, store_name, beer_id, customer_id, address)
        VALUES (:NEW.id, :NEW.store_name, :NEW.beer_id, :NEW.customer_id, :NEW.address);
    ELSIF DELETING THEN
        v_action := 'DELETE';
        INSERT INTO stores_h (id, store_name, beer_id, customer_id, address)
        VALUES (:OLD.id, :OLD.store_name, :OLD.beer_id, :OLD.customer_id, :OLD.address);
    END IF;
END;
/