CREATE OR REPLACE VIEW vw_customers_from_stores AS
SELECT c.id
      ,c.last_name  
      ,c.first_name
      ,s.store_name
      ,s.rate AS store_rate
FROM   stores s
       JOIN customers c
         ON s.customer_id = c.id
ORDER BY c.id
;