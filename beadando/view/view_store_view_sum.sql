CREATE VIEW vw_store_view_sum AS
SELECT c.id AS customerId
      ,c.first_name
      ,c.last_name
      ,c.age    
      ,s.store_name
      ,s.address
      ,s.rate
      ,b.id AS beerId
      ,b.beer_name
      ,b.beer_type    
      ,b.beer_piece
FROM   stores s
       JOIN customers c
         ON s.customer_id = c.id
    JOIN beers b
         ON s.beer_id = b.id
ORDER BY c.id, b.id
;