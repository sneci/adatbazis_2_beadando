CREATE VIEW vw_store_with_beers AS
SELECT b.id
      ,b.beer_name
      ,b.beer_type    
      ,b.beer_piece
	  ,s.store_name
    ,s.address
FROM   stores s
       JOIN beers b
         ON s.beer_id = b.id
ORDER BY b.id
;